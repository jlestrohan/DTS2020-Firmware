/**
 * --------------------------------------------------------------------------
 * irq_handler.c
 *
 *  Created on: Aug 22, 2020
 *      Author: Jack Lestrohan (c) Cobalt Audio - 2020
 * --------------------------------------------------------------------------
 */

//#include <stdint.h>
#include "configuration.h"
//#include "cmsis_os2.h"
#include "gpio.h"
//#include "printf.h"
//#include "adc.h"
//#include "i2c.h"
//#include "mcp23017.h"
//#include "multiplexer.h"
//#include "system_service.h"
//#include "gain_out_service.h"*/

#ifdef ACTIVATED_SERVICE_DEBUG
#include "debug_service.h"
osThreadId_t xDebugServiceTaskHandle;					/* extern */
#endif

//osMessageQueueId_t osMessageQueueSystemParams;			/* extern */
//osMessageQueueId_t osMessageQueueSystempowerBoardTempSensors; /* extern - fed by irq coming from ADC4 */

//osThreadId_t xMultiplexerIRQServiceTaskHandle;
//osThreadId_t xLcdDebugButtonsTaskHandle;				/* extern - debug LCD button */

/* GAIN OUT EXTERNS */
//osThreadId_t xGainOutLeftServiceTaskHandle;
//osThreadId_t xGainOutRightServiceTaskHandle;


/* system params */
//SystemParams_t sysparams;
//uint32_t adc_sysparams_buf;
//uint16_t adc4_temp1_buf;								/* extern */



//static SystemBoardTempsSensors_t boardsTempSensors;

/**
 * GPIO ISR callbacks
 * @param GPIO_Pin
 */
void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
	switch (GPIO_Pin)
	{

#ifdef DTS2020_DEBUG
	case USER_Btn_Pin:
		osThreadFlagsSet(xDebugServiceTaskHandle, THRFLAG_DEBUG_BTN);
		break;
#endif

		//case DEBUG_LCD_BTN_Pin:
		//osThreadFlagsSet(xLcdDebugButtonsTaskHandle, LCD_DEBUG_BUTTON_FWD_EVENT);
		//break;

	default:
		break;

	}
}

/**
 *
 * @param hadc
 */
void HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef *hadc)
{
	/**
	 * GAIN OUT LEFT
	 */
	//if (hadc->Instance == ADC3)
	//{
	//	osThreadFlagsSet(xGainOutLeftServiceTaskHandle, GAINOUT_LEFT_BUFFER_FULL_FLAG);
	//}
	/**
	 * GAIN OUT RIGHT
	 */
	//else if (hadc->Instance == ADC4)
	//{
	//	osThreadFlagsSet(xGainOutRightServiceTaskHandle, GAINOUT_RIGHT_BUFFER_FULL_FLAG);
	//}
}
