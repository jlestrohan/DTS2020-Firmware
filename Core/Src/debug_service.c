/**
 * --------------------------------------------------------------------------
 * debug_service.c
 *
 *  Created on: Aug 16, 2020
 *      Author: Jack Lestrohan (c) Cobalt Audio - 2020
 * --------------------------------------------------------------------------
 */

#include "printf.h"
#include "cmsis_os2.h"
#include "configuration.h"
#include "debug_service.h"
#include <stdlib.h>

#define STM32_UUID ((uint32_t *) 0x1FF0F420 )

/**
 * Definitions for the tasks
 */
static osStaticThreadDef_t xDebugServiceTaControlBlock;
static uint32_t xDebugServiceTaBuffer[256];
osThreadId_t xDebugServiceTaskHandle;

static const osThreadAttr_t xDebugServiceTa_attr = {
		.name = "debug_service",
		.stack_mem = &xDebugServiceTaBuffer[0],
		.stack_size = sizeof(xDebugServiceTaBuffer),
		.cb_mem = &xDebugServiceTaControlBlock,
		.cb_size = sizeof(xDebugServiceTaControlBlock),
		.priority = (osPriority_t) OSTASK_PRIORITY_DEBUG_SERVICE
};

/**
 * Main debug task
 * @param argument
 */
__NO_RETURN void vDebugServiceTask(void *argument)
{
	UNUSED(argument);

	//TODO: https://k1.spdns.de/Develop/Hardware/STM32/mbed/mbed-os-example-blinky/mbed-os/platform/mbed_stats.c
	// https://k1.spdns.de/Develop/Hardware/STM32/mbed/mbed-os-example-blinky/mbed-os/platform/mbed_stats.h

	for (;;)
	{
		osThreadFlagsWait(THRFLAG_DEBUG_BTN, osFlagsWaitAny, osWaitForever); // Wait forever until thread flag 1 is set.

		uint32_t thread_n = osThreadGetCount();
		osThreadId_t *threads;

		printf("------------------------------------\n\r");
		printf("TASKS ENUMERATION\n\r");
		printf("* Number of active threads: %lu\n\r", osThreadGetCount());

		threads = pvPortMalloc(sizeof(osThreadId_t) * thread_n);
		osKernelLock();
		thread_n = osThreadEnumerate(threads, thread_n);
		printf("----------------------------------------\n\r");
		printf("| Task | Pri | Task Name        | Stack |\n\r");
		printf("----------------------------------------\n\r");
		for (uint8_t i = 0; i < thread_n; i++) {
			uint32_t stack_size = osThreadGetStackSpace(threads[i]);
			printf("|  %0*d | %0*d | %0*s | %0*lub  |\r\n",
					3, i,
					3, osThreadGetPriority(threads[i]),
					16, osThreadGetName(threads[i]),
					3, stack_size);

		}
		printf("----------------------------------------\n\r");
		vPortFree(threads);
		osKernelUnlock();

		printf("* Chip Id: %.8x %.8x %.8x\n\r", STM32_UUID[0], STM32_UUID[1], STM32_UUID[2]);
		printf("* Version %0.2f compiled %s on %s\n\r", DTS2020_FIRMWARE_VERSION, __TIME__, __DATE__);

		osDelay(10);
	}
	osThreadTerminate(NULL);
}

/**
 * Main debug service initialization routine
 * @return
 */
HAL_StatusTypeDef hInitDebugService()
{
	printf("Initializing debug service... ");

	xDebugServiceTaskHandle = osThreadNew(vDebugServiceTask, NULL, &xDebugServiceTa_attr);
	if (xDebugServiceTaskHandle == NULL) {
		printf("failure.. (debug service task not created)\n\r");
		return HAL_ERROR;
	}

	return HAL_OK;
}
