/* USER CODE BEGIN Header */
/**
 ******************************************************************************
 * File Name          : freertos.c
 * Description        : Code for freertos applications
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
 * All rights reserved.</center></h2>
 *
 * This software component is licensed by ST under Ultimate Liberty license
 * SLA0044, the "License"; You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *                             www.st.com/SLA0044
 *
 ******************************************************************************
 */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "FreeRTOS.h"
#include "task.h"
#include "main.h"
#include "cmsis_os.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include <stdlib.h>
#include "printf.h"
#include "configuration.h"

#ifdef DTS2020_DEBUG
#include "debug_service.h"		/* gives FreeRTOS tasks and memory stats with a button press (BA/B1_DBG) */
#endif
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
/* USER CODE BEGIN Variables */

/* USER CODE END Variables */
/* Definitions for defaultTask */
osThreadId_t defaultTaskHandle;
const osThreadAttr_t defaultTask_attributes = {
  .name = "defaultTask",
  .priority = (osPriority_t) osPriorityNormal,
  .stack_size = 128 * 4
};

/* Private function prototypes -----------------------------------------------*/
/* USER CODE BEGIN FunctionPrototypes */

/* USER CODE END FunctionPrototypes */

void StartDefaultTask(void *argument);

void MX_FREERTOS_Init(void); /* (MISRA C 2004 rule 8.1) */

/* Hook prototypes */
void vApplicationStackOverflowHook(xTaskHandle xTask, signed char *pcTaskName);

/* USER CODE BEGIN 4 */
void vApplicationStackOverflowHook(xTaskHandle xTask, signed char *pcTaskName)
{
	/* Run time stack overflow checking is performed if
   configCHECK_FOR_STACK_OVERFLOW is defined to 1 or 2. This hook function is
   called if a stack overflow is detected. */
	printf("Stack overflow at %s\n\r", pcTaskName);
}
/* USER CODE END 4 */

/**
  * @brief  FreeRTOS initialization
  * @param  None
  * @retval None
  */
void MX_FREERTOS_Init(void) {
  /* USER CODE BEGIN Init */
	/* MAIN INITIALIZATION COMES HERE */

	/* for now we disable the watchdog */
	IWDG->KR = 0x0000;

	uint8_t uPrintfInit();

	printf ("---------------------------------------------\n\r");
	printf ("DTS20200 Core firmware v%0.2f starting...\n\r", DTS2020_FIRMWARE_VERSION);
	printf ("---------------------------------------------\n\r");
	printf("Starting up the different services now ... \n\n\r");

#ifdef DEBUG_ACTIVATED_SERVICE_MULTIPLEXER
	/**
	 * MULTIPLEXER SERVICE
	 */
	printf("Initializing Multiplexers control service... ");
	if (hMultiplexerServiceInit() != HAL_OK) {
		printf("error! Exiting program!\n\r");
		exit(1);
	}
	printf("success!\n\r");
#endif

#ifdef DEBUG_ACTIVATED_SERVICE_INGAIN
	/**
	 * IN GAIN SERVICE
	 */
	printf("Initializing Input Gain control service... ");
	if (hGainInServiceInit() != HAL_OK) {
		printf("error! Exiting program!\n\r");
		exit(1);
	}
	printf("success!\n\r");
#endif

#ifdef DEBUG_ACTIVATED_SERVICE_OUTGAIN
	/**
	 * OUT GAIN SERVICE
	 */
	printf("Initializing Output gain control service... ");
	if (hGainOutServiceInit() != HAL_OK) {
		printf("error! Exiting program!\n\r");
		exit(1);
	}
	printf("success!\n\r");
#endif

#ifdef DEBUG_ACTIVATED_SERVICE_HPF
	/**
	 * HPF SERVICE
	 */
	if (hHPFServiceInit() != HAL_OK) {
		printf("error! Exiting program!\n\r");
		exit(1);
	}
	printf("success!\n\r");
#endif

#ifdef ACTIVATED_SERVICE_DEBUG
	/**
	 * DEBUG SERVICE
	 */
	if (hInitDebugService() != HAL_OK) {
		printf("error! Exiting program!\n\r");
		exit(1);
	}
	printf("success!\n\r");
#endif

#ifdef DEBUG_ACTIVATED_SERVICE_SYSTEM
	/**
	 * SYSTEM SERVICE
	 */
	printf("Initializing Service Parameters Control service... ");
	if (hSystemServiceInit() != HAL_OK) {
		printf("error! Exiting program!\n\r");
		exit(1);
	}
	printf("success!\n\r");
#endif

#ifdef DEBUG_ACTIVATED_SERVICE_LCD
	/**
	 * LCD DEBUG SERVICE
	 */
	if (hInitLcdDebugService() != HAL_OK) {
		printf("LCD Debug service could not be initializing.. Skipping!\n\r");
	} else printf("LCD Debug service detected and initialized.\n\r");
#endif

	printf("--------------------------------------------\n\r");
	printf("--> ALL Services Initialization is complete!\n\r");

	/* watchdog ? */
#if defined DTS2020_WATCHDOG_ENABLED
	printf("Initializing IDWG\n\r");
	// 1. Enable the IWDG by writing 0x0000 CCCC in the IWDG_KR register.
	IWDG->KR = 0xCCCC;
#else
	IWDG->KR = 0x0000;
#endif

  /* USER CODE END Init */

  /* USER CODE BEGIN RTOS_MUTEX */
	/* add mutexes, ... */
  /* USER CODE END RTOS_MUTEX */

  /* USER CODE BEGIN RTOS_SEMAPHORES */
	/* add semaphores, ... */
  /* USER CODE END RTOS_SEMAPHORES */

  /* USER CODE BEGIN RTOS_TIMERS */
	/* start timers, add new ones, ... */
  /* USER CODE END RTOS_TIMERS */

  /* USER CODE BEGIN RTOS_QUEUES */
	/* add queues, ... */
  /* USER CODE END RTOS_QUEUES */

  /* Create the thread(s) */
  /* creation of defaultTask */
  defaultTaskHandle = osThreadNew(StartDefaultTask, NULL, &defaultTask_attributes);

  /* USER CODE BEGIN RTOS_THREADS */
	/* add threads, ... */
  /* USER CODE END RTOS_THREADS */

  /* USER CODE BEGIN RTOS_EVENTS */
	/* add events, ... */
  /* USER CODE END RTOS_EVENTS */

}

/* USER CODE BEGIN Header_StartDefaultTask */
/**
 * @brief  Function implementing the defaultTask thread.
 * @param  argument: Not used
 * @retval None
 */
/* USER CODE END Header_StartDefaultTask */
void StartDefaultTask(void *argument)
{
  /* USER CODE BEGIN StartDefaultTask */
	/* Infinite loop */
	for(;;)
	{
		osDelay(1);
	}
  /* USER CODE END StartDefaultTask */
}

/* Private application code --------------------------------------------------*/
/* USER CODE BEGIN Application */

/* USER CODE END Application */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
