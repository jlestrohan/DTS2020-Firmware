/**
 * --------------------------------------------------------------------------
 * debug_service.h
 *
 *  Created on: Aug 16, 2020
 *      Author: Jack Lestrohan (c) Cobalt Audio - 2020
 * --------------------------------------------------------------------------
 */

#ifndef INC_DEBUG_SERVICE_H_
#define INC_DEBUG_SERVICE_H_

#include "stm32f7xx_hal.h"

#define THRFLAG_DEBUG_BTN		1 << 5

extern osThreadId_t xDebugServiceTaskHandle;

/**
 * Very low priority debug task
 * @return
 */
HAL_StatusTypeDef hInitDebugService();

#endif /* INC_DEBUG_SERVICE_H_ */
